import React, { useEffect, useState } from "react";
import { api } from "../../services/api";

import { useParams } from "react-router-dom";

const VisualizarCliente = (navigation) => {
    let params = useParams();

    console.log(navigation.state.params);
  

    const [clientes, setClientes] = useState([]);
  
    useEffect(() => {
        getData(1);
    },[]);

    async function getData(id){
        const response = await api.get(`/clientes/${id}`);
        setClientes(response.data);
    }

    return (
        <div>
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          />
          <div className="card">
            <h3>{clientes.nome}</h3>
            <p>ID: {clientes.id}</p>
            <p>Peso: {clientes.peso}</p>
            <p>Altura: {clientes.altura}</p>
            <p>Sexo: {clientes.sexo}</p>
            <p>Imc: {clientes.imc}</p>
          </div>
        </div>
    );
}

export default VisualizarCliente;