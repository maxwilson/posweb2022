import React from "react";
import { Container } from "reactstrap";
import VisualizarCliente from "../VisualizarCliente";

export function App(){
    return(
        <Container>
            <VisualizarCliente />
        </Container>
    );
}